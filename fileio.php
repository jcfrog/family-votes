<?php

$dataPath = "data";
$moviesPath="$dataPath/movies";
$votesPath="$dataPath/votes";
$archiveMoviesPath = "$dataPath/archive/movies";
$archiveVotesPath = "$dataPath/archive/votes";

function makeSureExists($path){
    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }
}

makeSureExists($archiveMoviesPath);
makeSureExists($archiveVotesPath);
makeSureExists($moviesPath);
makeSureExists($votesPath);



function human_to_machine($human_name) {
    return strtolower(preg_replace(array(
      '/[^a-zA-Z0-9]+/',
      '/-+/',
      '/^-+/',
      '/-+$/',
    ), array('-', '-', '', ''), $human_name));
}


function archive($id){
    global $archiveMoviesPath , $archiveVotesPath, $moviesPath, $votesPath;
    


    $counter = 0 ;
    $errCounter = 0 ;
    foreach (glob("$moviesPath/$id.txt") as $filename) {
        $bn = basename($filename);
        if (rename("$moviesPath/$bn","$archiveMoviesPath/$bn")){
            $counter++;
        }else{
            $errCounter++;
        }
    }
    foreach (glob("$votesPath/$id.*.txt") as $filename) {
        $bn = basename($filename);
        if (rename("$votesPath/$bn","$archiveVotesPath/$bn")){
            $counter ++ ;
        }else{
            $errCounter++;
        }
    }
    echo("{\"nbmoved\":\"$counter\",\"nberr\":\"$errCounter\",\"errMsg\":\"\"}");

}

function restore($id){
    global $archiveMoviesPath , $archiveVotesPath, $moviesPath, $votesPath;

    $counter = 0 ;
    $errCounter = 0 ;
    foreach (glob("$archiveMoviesPath/$id.txt") as $filename) {
        $bn = basename($filename);
        if (rename("$archiveMoviesPath/$bn","$moviesPath/$bn")){
            $counter++;
        }else{
            $errCounter++;
        }
    }
    foreach (glob("$archiveVotesPath/$id.*.txt") as $filename) {
        $bn = basename($filename);
        if (rename("$archiveVotesPath/$bn","$votesPath/$bn")){
            $counter ++ ;
        }else{
            $errCounter++;
        }
    }
    echo("{\"nbmoved\":\"$counter\",\"nberr\":\"$errCounter\",\"errMsg\":\"\"}");
}




if ( isset($_POST['action'])){
   
    $errMsg = "" ;

    if(($_POST['action']=='load')){

        $rootDir = $moviesPath ;
        if ($_POST['filter']=="archive"){
            $rootDir = $archiveMoviesPath ;
        }

        $data = [];
        $ids = [];
        $votes = [];
        // scan movies dir for files
        $files = array_diff(scandir($rootDir), array('..', '.'));
        
        foreach($files as $fn){
            $p = "$rootDir/$fn" ;
            if (is_file($p)){
                $f = fopen($p,"rt");
                if ($f){
                    $content = fread($f,filesize($p));
                    $data[] = $content;
                    $ids[] = "\"".basename($fn,".txt")."\"";
                    fclose($f);
                }else{
                    $errMsg = "cant read file $fn";
                }
            }
        }

        $rootDir = $votesPath ;
        if ($_POST['filter']=="archive"){
            $rootDir = $archiveVotesPath ;
        }

        // scan votes dir
        $files = array_diff(scandir($rootDir), array('..', '.'));
        $votes = [];
        foreach($files as $fn){
            $p = "$rootDir/$fn" ;
            if ($d = file_get_contents($p)){        
                $votes[basename($p,".txt")] = json_decode($d) ;
            }
        }

        echo("{\"movies\":[".join(",",$data)."],\"ids\":[".join(",",$ids)."],\"errMsg\":\"$errMsg\",\"votes\":".json_encode($votes)."}");
    }

    if(($_POST['action']=='addmovie')){

        $creationtime = time();
        $lastmodif = time();
        if (isset($_POST["creationtime"])){
            if(!empty($_POST["creationtime"]))
                $creationtime = $_POST["creationtime"] ;
        }
        $out = '{
            "name" : "'.$_POST["movie-name"].'",
            "description" : "'.$_POST["comment"].'",
            "creationtime" : '.$creationtime.',
            "lastmodification" : '.$lastmodif.',
            "links" : [
                {
                    "nature" : "trailer",
                    "link" : "'.$_POST["link0"].'"
                },
                {
                    "nature" : "thumbnail",
                    "link" : "'.$_POST["thumbnail"].'"
                }
            ]
        }';     


        $id = human_to_machine($_POST["movie-name"]);

        if (strlen($_POST["movie-id"])>0){
            // the name was already set
            $id = $_POST["movie-id"];
        }

        file_put_contents($moviesPath."/".$id.".txt",$out);

        header("Location: index.php");
        exit();
    }


    if(($_POST['action']=='loadbyid')){
        $fn = $moviesPath."/".$_POST['id'].".txt";
        if ($data = file_get_contents($fn)){
            echo("{\"data\":$data,\"errMsg\":\"\"}");
        }else{
            echo("{\"errMsg\":\"file $fn not found...\"}");
        }
    }


    if ($_POST['action']=='archive'){
        archive($_POST['id']);
    }
    if ($_POST['action']=='restore'){
        restore($_POST['id']);
    }

    if ($_POST['action']=='save-vote'){
        if (file_put_contents(
            "$votesPath/".$_POST['id'].".".$_POST['user'].".txt",
            "{\"score\" : ".$_POST['value'].",\"comment\" : \"\"}"
        )){
            echo("{\"errMsg\":\"\"}");
        }
        else{
            echo("{\"errMsg\":\"Error saving vote for ".$_POST['id']."\"}");
        }
    }

}

?>