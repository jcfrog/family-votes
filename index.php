<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Family's choice</title>

    <link rel="stylesheet" href="bootstrap-5.0.0-alpha1-dist/css/bootstrap.min.css">
    <script src="bootstrap-5.0.0-alpha1-dist/js/bootstrap.min.js"></script>
    <script src="bootstrap-5.0.0-alpha1-dist/popper.min.js"></script>
    <script src="js/jquery-3.2.1.min.js"></script>

    <script src="js/users.js"></script>
    <script src="js/vuejs/vue.js"></script>

    <script src="js/common.js"></script>

    <link rel="stylesheet" href="css/main.css">

    <link rel="icon" type="image/png" href="i/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="i/favicon-16x16.png" sizes="16x16" />


</head>


<body>

    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link active" href="index.php">Films</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="index.php?p=archive">Archives</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="editmovie.php">Add movie</a>
        </li>
    </ul>

    <div id="users"></div>

    <div class="container">
        <?php    
    
    if (isset($_GET["filterinit"])){
        echo("<script>var filterInit=\"".$_GET["filterinit"]."\";</script>");
    }
        
    if (isset($_GET["p"]) && $_GET["p"]=="archive"){
        echo("<script>var page='archive';</script>");
        echo("<h1><span class='t'>Archives</span></h1>");
    }else{
        echo("<script>var page='movies';</script>");
        echo("<h1><span class='t'>Films</span></h1>");
    }
    ?>
        <div id="mainscroller">
            <div>Filter : <input type="text" name="searchmovie" id="searchmovie" value="" placeholder="search for ..."></input> <button id="clear-search" type="button" class="btn btn-primary btn-sm">Clear</button></div>
            <div id="movies-list">No movies...</div>
        </div>
    </div>
</body>



<script>
function archiveMovie(id) {
    $.post("./fileio.php", {
            action: "archive",
            id: id
        },
        function(json) {
            // the output of the response is now handled via a variable call 'results'
            var data = JSON.parse(json);
            //console.log(data);
            reload(page);
        });

}

function restoreMovie(id) {
    $.post("./fileio.php", {
            action: "restore",
            id: id
        },
        function(json) {
            // the output of the response is now handled via a variable call 'results'
            var data = JSON.parse(json);
            //console.log(data);
            reload(page);
        });

}

function compressedId(id){
    return id.split('-').join('');
}

var votes = {};
var computations = null;
function compareDesc(a, b) {
  if (a.score > b.score) return -1;
  if (a.score < b.score) return 1;
  return 0;
}
function compareAsc(a, b) {
  if (a.score < b.score) return -1;
  if (a.score > b.score) return 1;
  return 0;
}
function computeScores() {
    var scores = [];
    $(".movie-entry").each(function() {
        var id = $(this).attr("id");
        var accu = 0 ;
        var n = 0;
        for (u in users) {                            
            accu += votes[id][u].score * usersApp.coeffs[u];
            n += usersApp.coeffs[u];
        }
        scores.push({ id : id , score : Math.round(accu*100/n)/100 });
    });

    // reorder ids
    scores.sort(compareDesc); 

    //reorder divs
    for (var i = 1 ; i < scores.length ; i++){
        $("#"+scores[i].id).insertAfter("#"+scores[i-1].id);
    }

    // update view fields
    for (var i = 0 ; i < scores.length ; i++){
        var id = scores[i].id;
        $("#"+id+" .rank-value").text(i + 1) ;
        $("#"+id+" .score-value").text(scores[i].score) ;
    }

    //console.log(scores);
}

function sortByDate(){
    var dates = [] ;
    $(".movie-entry").each(function() {
        var id = $(this).attr("id");
        var ts = $("#"+id+"-"+"lastmodification-value").val();
        dates.push({ id : id , ts : ts });
    });

    dates.sort(function(a,b){
        if (a.ts < b.ts) return 1;
        if (a.ts > b.ts) return -1;
        return 0;        
    }); 

    //reorder divs
    for (var i = 1 ; i < dates.length ; i++){
        $("#"+dates[i].id).insertAfter("#"+dates[i-1].id);
    }
    
}

function reload(filter = '') {
    $("#movies-list").html("Scanning...");
    $.post("./fileio.php", {
            action: "load",
            filter: filter
        },
        function(json) {

            votes = {};

            //console.log(json);
            var data = JSON.parse(json);
            var html = "";

            // movies
            n = data.movies.length;
            html += data.movies.length + " movie" + (n > 1 ? "s" : "") + " found";
            if (n > 1) 
                html += " (See last modifications : <a href='javascript:sortByDate();'>Sort by date</a>)";

            for (var i = 0; i < data.movies.length; i++) {
                var m = data.movies[i];
                if(!m.creationtime) m.creationtime = 0 ;
                if(!m.lastmodification) m.lastmodification = 0 ;
                html += "<div class='movie-entry' id='" + data.ids[i] + "' name='" + m.name + "'>";
                html += "<h1 class='movie-title'>" + m.name;

                
                if (filter == "archive") {} else {
                    html += " <a href='editmovie.php?id=" + data.ids[i] +
                    "'><img class='fi' src='i/feather/edit.svg'></a>";
                }
                html += " <a href='?filterinit=" + m.name +
                    "'><img class='fi' src='i/feather/share-2.svg'></a>";
                html += "</h1>";
                
                html += "<div class='rank-list-entry'>Rank : <span class='rank-value'></span>, score : <span class='score-value'></span></div>";
                html += "<div class='movie-score'>";
                html += "</div>";


                if (m.links) {
                    var nblinks = 0;
                    var tUrl = "";
                    var vUrl = "";
                    for (var l = 0; l < m.links.length; l++) {
                        var item = m.links[l];
                        if (item.link.length > 0) {
                            if (item.nature == "trailer") vUrl = item.link;
                            if (item.nature == "thumbnail") tUrl = item.link;
                            nblinks++;
                        }
                    }
                    if (tUrl.length > 0) {
                        if (vUrl.length > 0)
                            html += "<a target=\"_blank\" href='" + vUrl + "'><img class='tn' src='" + tUrl +
                            "'></a>";
                        else
                            html += "<img class='tn' src='" + tUrl + "'>";


                    }
                    if (m.description && m.description.length > 0)
                        html += "<div class='movie-details'>" + m.description + "</div>";
                        html += "<div class='clear'></div>";
                        html += "<input type='hidden' id='"+data.ids[i]+"-creationtime-value' value="+m.creationtime+">";
                        html += "<input type='hidden' id='"+data.ids[i]+"-lastmodification-value' value="+m.lastmodification+">";
                        html += "<div class='timings' >Created : <span class='created-value'>"+timeConverter(m.creationtime)+"</span>, modified : <span class='modified-value'>"+timeConverter(m.lastmodification)+"</span></div>";
                }
                html += "</div>";
            }


            // processing votes
            for (const v in data.votes) {
                // console.log(v);
                var t = v.split(".");
                var id = t[0];
                var user = t[1];
                if (!votes[id]) votes[id] = [];
                votes[id][user] = data.votes[v];
            }


            $("#movies-list").html(html);

            // add archive/restore buttons + votes items
            $(".movie-entry").each(function() {
                var id = $(this).attr("id");
                var name = $(this).attr("name");

                var button;

                if (filter == "archive") {
                    button = $(
                        '<svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-refresh-ccw"><polyline points="1 4 1 10 7 10"></polyline><polyline points="23 20 23 14 17 14"></polyline><path d="M20.49 9A9 9 0 0 0 5.64 5.64L1 10m22 4l-4.64 4.36A9 9 0 0 1 3.51 15"></path></svg>'
                    );
                    button.addClass("jc-icon jc-icon-green clickable").click(function() {
                        if (confirm("Restore " + name + "?")) {
                            restoreMovie(id);
                        }
                    });
                } else {
                    button = $(
                        '<svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>'
                    );
                    button.addClass("jc-icon jc-icon-red clickable").click(function() {
                        if (confirm("Archive " + name + "?")) {
                            archiveMovie(id);
                        }
                    });
                }
                $(this).find(".movie-title").append(button);

                var $this = $(this);
                // adding votes
                for (u in users) {                            
                    (function (u) {
                        var container = $("<div></div>").addClass("vote-user-container").css("background-color",users[u].color+"66");
                        var vote = $("<div id='uvctrl-"+u+"'></div>").addClass("vote-user-value").css("background-color",users[u].color+"FF");

                        container.click(function(e){
                            var h = $(this).height();
                            var val = h-e.originalEvent.layerY ;
                            val = Math.round(10*val/h);
                            setVote(id,u,val);
                        });

                        container.append(vote);
                        $this.find(".movie-score").append(container);
                    }(u))
                }   
            })

            // update votes display
            updateAllVotes();

            // compute current 
            //computeScores();
            sortByDate(); // sort by dates by default
            if(filterInit){
                $("#searchmovie").val(filterInit);
                filterList();
            }            
            
        }
    );
}

function updateAllVotes(){
    $(".movie-entry").each(function() {
        var id = $(this).attr("id");
        for (u in users) {                            
            var v = 5; // default if no vote
            if (votes[id] && votes[id][u]){
                v = votes[id][u].score;
            }
            updateVoteView(id,u,v);
        }
    });
}

// user info
// msg colors
const MC_INFO = "#cf0";
const MC_ERROR = "#f00";
function informUser(msg, col = MC_INFO){
    var $infoMsg = $("<div class='user-info'>"+msg+"</div>").css("background-color",col);
    $("#movies-list").append($infoMsg);
    setTimeout(function() { $infoMsg.remove(); }, 1500);
}

// display vote
function displayVote(movieId,user,value){
    $("#"+movieId+" .movie-score #uvctrl-"+user).css("height",""+value+"0%");
}
// update local votes struct and display
function updateVoteView(movieId,user,value){
    if (!votes[movieId]) votes[movieId] = [];
    if (!votes[movieId][user]) votes[movieId][user] = {};
    votes[movieId][user].score = value;
    votes[movieId][user].comment = "";
    displayVote(movieId,user,value);
}
// save vote to file and update view if success
function setVote(movieId,user,value){
    //console.log(movieId,user,value);
    $.post("./fileio.php", {
            action: "save-vote",
            id: movieId,
            user :user,
            value : value
        },
        function(json) {
            //console.log(json);
            var data = JSON.parse(json);
            if (data.errMsg.length == 0){
                updateVoteView(movieId,user,value);
                //computeScores();
                informUser(user+"'s vote "+value+" saved!");
            }else{
                informUser("ERROR : "+user+"'s vote not saved!",MC_ERROR);
            }
        }
    );
}

var usersApp;


function populateUsers() {
    var d = $("<div class='users-centering-container'></div>");
    for (u in users) {        
        var cell = $("")
        //d.append("<div class='col col-2'><img class='user-banner' src='"+users[u].avatar+"'>"+users[u].name+"</div>");
        var userDiv = $("<div id='user-" + u + "' class='user-div clickable'><div class='user-icon' style='background-color:" + users[u]
            .color +
            "'>{{ coeffs."+compressedId(u)+" }}</div><div class='user-name'>" + users[u].name + "</div></div>");
        d.append(userDiv);

    }
    $("#users").append(d);
    var b = $("<button id='sort-movies' type='button' class='btn btn-primary btn-sm'>Sort</button>");
    $("#users").append(b);
    $("#users").append("<div class='clear'></div>");
    
}


var selectedUser = "";
function selectUser(user){
    selectedUser = user ;
    for (u in users) {
        if (u == user) $("#user-"+u).toggleClass("selected-user");
        else $("#user-"+u).removeClass("selected-user");
    }
    if ( ! $("#user-"+selectedUser).hasClass("selected-user")) selectedUser = "" ;
    //console.log("selectedUser = "+selectedUser);
}

function filterList(){
    var filter = $("#searchmovie").val().toLowerCase();
    $(".movie-entry").each(function() {
        var name = $(this).attr("name").toLowerCase();
        if ((filter.length == 0) || (name.indexOf(filter)>=0)){
            $(this).show();
        }else{
            $(this).hide();
        }
    });
}

$(document).ready(function() {


    if (page == "movies")
        populateUsers();        
    reload(page);

    

    var c = {};
    for (u in users) {        
        c[compressedId(u)] = 1 ;
    }
    usersApp = new Vue({
        el: '#users',
        data: {
            coeffs: c,
        }
    });

    for (u in users) {        
        var ur = ""+u;
        (function (u) {
            $("#user-"+u).click(function(){
                selectUser(u);
            });
        }(u))
    }

    $("#searchmovie").keyup(function(){
        filterList();
    });
    $("#clear-search").click(function(){
        $("#searchmovie").val("");
        filterList();
    });
    $(this).keydown(function( event ) {

        var bSomeChange = false ;
        // console.log(event.which);
        if (selectedUser.length > 0){
            if(event.key == "+"){ 
                usersApp.coeffs[selectedUser] = Math.min(usersApp.coeffs[selectedUser]+1,9);
                bSomeChange = true ;
            }
            if(event.key == "-"){ 
                usersApp.coeffs[selectedUser] = Math.max(usersApp.coeffs[selectedUser]-1,0);
                bSomeChange = true ;
            }
            if(event.key == "-"){ 
                usersApp.coeffs[selectedUser] = Math.max(usersApp.coeffs[selectedUser]-1,0);
                bSomeChange = true ;
            }
            if( (event.which >= 96) & (event.which <= 105)){ // numpad
                usersApp.coeffs[selectedUser] = event.which - 96 ;
                selectUser("");
                bSomeChange = true ;
            }
            if( (event.which >= 48) & (event.which <= 57)){ // main keyboard
                usersApp.coeffs[selectedUser] = event.which - 48 ;
                selectUser("");
                bSomeChange = true ;
            }
        }

        if (bSomeChange) 
            computeScores()

    });
        
    $("#sort-movies").click(function(){computeScores()});


});
</script>

</html>