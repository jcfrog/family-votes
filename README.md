![alt tag](i/doc/home.png)


## What does it do ?


It is supposed to help the family to answer the daily question : "what are we watching tonight?". Anyone in the house can add propositions, everyone votes, and the system will decide depending on those present (and coeffcients).

It is to be installed on a local server in a safe place, there is no identification, no protection, anyone can change all movies and votes, you have to be in a trusted area.

It uses PHP, no data base.

You can :

* add/edit/archive movies, series, anything to be proposed to the family
* for each movie you can add a link (generally to a trailer), and a thumbnail url
* vote for a movie by just clicking on the corresponding color bar 
* sort the movies to get the best choice according to :
    * everyone's votes
    * people staying tonight
    * coefficients : put 1 for those present, 0 for the absents, and if you want to be nice to mummy who has let us watch Star Wars yesterday, we can even put a high coefficient for her so that she will be rewarded :)  

## License

MIT

## Install

### Setup your family / group

Edit [users file](js/users.js) (path = `js/users.js`) to change ids and names, and colors, according to your family or group. 

### Data 

The data is stored in small files, no database. Everything is in `data`, so once the project is cloned, create a `data` directory and just make sure `data` is **writable**.

### Youtube API

If you want to use the "Search trailer" feature to load trailers and thumbnails, you'll have to get a Youtube API key, open [ytapikey.js](js/ytapikey.js) (path = `js/ytapikey.js`) for mor details

![alt tag](i/doc/addmovie.png)
