// given as an example, you can add and remove any user, just be sure to respect the data structure
const users  = {
    paul : {
        "name" : "Paul",
        "color" : "#ff4f4f"
    },
    jean : {
        "name" : "Jean",
        "color" : "#ffac2c"
    },
    marie : {
        "name" : "Marie",
        "color" : "#ff01bb"
    },
    mummy : {
        "name" : "Mummy",
        "color" : "#92db3d"
    },
    daddy : {
        "name" : "Daddy",
        "color" : "#5f60ab"
    }
}