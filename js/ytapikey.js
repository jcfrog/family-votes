// When you add a movie, you might want to provide a trailer, and a thumbnail
// You can add trailer and thumbnail URLs manualy, but you can also use some automation. 
// I chose to use the Youtube API, because... what else?... :/
// This is used in editmovie.php, link "Search trailer"
// To be able to use this API, you'll need a "YouTube Data API v3" key, 
// it's free for such a use case as we don't make many requests

// you can get this key from there  : https://developers.google.com/youtube/v3/getting-started


var myYTapiKey = "" ;



// for my family we search trailers in french or original version with subtitles, change request param to suit your needs

var searchKeyWords = "trailer+vostfr+bande+annonce";