<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="bootstrap-5.0.0-alpha1-dist/css/bootstrap.min.css">
    <script src="bootstrap-5.0.0-alpha1-dist/js/bootstrap.min.js"></script>
    <script src="bootstrap-5.0.0-alpha1-dist/popper.min.js"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/common.js"></script>

    <link rel="stylesheet" href="css/main.css">


    <title>Edit movie</title>
</head>

<?php
    $movieID = "";
    if (isset($_GET["id"])){
        $movieID = $_GET["id"] ;
    }
    echo('<script>var movieId="'.$movieID.'"</script>');                
?>

<script src="js/ytapikey.js"></script>


<script>





function chooseLink(url) {
    $("#link0").val(url);
}

function chooseThumbnail(url) {
    $("#thumbnail").val(url);
}


// search on youtube
// default thumbnail : src="https://img.youtube.com/vi/<id>/default.jpg"

function search() {

    var t = $("#movie-name").val();
    window.open("https://www.youtube.com/results?search_query=bande+annonce+vf+"+t);
    
    return ;


    // youtube api v3 
    /*if (myYTapiKey.length == 0) {
        $("#dumper").html("<p>Sorry you need to setup your Youtube API key for this feature, please see js/ytapikey.js file.</p>");
        return;
    }
    var t = $("#movie-name").val();

    $.ajax({
        url: 'https://www.googleapis.com/youtube/v3/search?part=snippet&q=' + t +
            '+'+searchKeyWords+'&type=video&key=' +
            myYTapiKey,
        success: function(data) {
            console.log(data);
            html = "";
            //process the JSON data etc
            if (data.items.length) {
                for (var i = 0; i < data.items.length; i++) {
                    var item = data.items[i];
                    var videoUrl = "https://www.youtube.com/watch?v=" + item.id.videoId;
                    html += "<div>";
                    html += "<img src='" + item.snippet.thumbnails.default.url + "'>";
                    html += "<p><a href='" + videoUrl +
                        "' target='_blank'>Watch it</a> • <a href='javascript:chooseLink(\"" + videoUrl +
                        "\")'>Use this for 'link'</a> • <a href='javascript:chooseThumbnail(\"" + item
                        .snippet.thumbnails.default.url +
                        "\")'>Use this for 'thumbnail'</a></p>"
                    html += "</div>";
                }
            } else {
                html = "Sorry, found nothing ... :("
            }
            $("#dumper").html(html);
        },
        error: function(data){
            console.log(data.responseJSON.error.message);
            $("#dumper").html("<p>"+data.responseJSON.error.message+"</p>");
        }
    })*/
}

function reloadMovie(id) {
    $.post("./fileio.php", {
            action: "loadbyid",
            id: id
        },
        function(json) {
            var msg = JSON.parse(json);

            console.log("reloadmovie data", msg.data);

            if (msg.errMsg.length == 0) {
                
                $("#movie-name").val(msg.data.name);
                $("#comment").val(msg.data.description);
                $("#creationtime").val(msg.data.creationtime);
                $("#lastmodification").val(msg.data.lastmodification);

                if (msg.data.creationtime){
                    $("#creationtime-str").text(timeConverter(msg.data.creationtime));
                }
                if (msg.data.lastmodification){
                    $("#lastmodification-str").text(timeConverter(msg.data.lastmodification));
                }
                
                if (msg.data.links) {
                    for (var i = 0; i < msg.data.links.length; i++) {
                        var l = msg.data.links[i];
                        if (l.nature == "trailer") $("#link0").val(l.link);
                        if (l.nature == "thumbnail") {
                            $("#thumbnail").val(l.link);
                            $("#thumbnail-img").attr("src",l.link);
                        }
                    }
                }
            }
        }
    );
}
$(document).ready(function() {
    
    $("#thumbnail").change(function(){
        $("#thumbnail-img").attr("src",$("#thumbnail").val());
    });
    $("#link0").change(function(){
        var link = $("#link0").val();

        const regex = /(youtube\.com\/watch\?v\=)([A-Za-z0-9_\-]{11})/;
        let m;

        if ((m = regex.exec(link)) !== null) {   
            $("#thumbnail").val("https://img.youtube.com/vi/"+m[2]+"/default.jpg");
            $("#thumbnail-img").attr("src",$("#thumbnail").val());
        }

    });
    

    if (movieId.length > 0) {
        reloadMovie(movieId);
    }
});
</script>

<body>
    <div class="container">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link active" href="index.php">Films</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?p=archive">Archives</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="editmovie.php">Add movie</a>
            </li>
        </ul>

        <div class="row">
            <div class="formcontainer col-md-6">
                <form action="fileio.php" method="post">
                    <input type="hidden" id="action" name="action" value="addmovie">
                    <input type="hidden" id="movie-id" name="movie-id" value="<?php echo($movieID); ?>">
                    <div class="mb-3">
                        <label class="form-label" for="movie-name">Movie title </label>
                        <input id="movie-name" class="form-control" name="movie-name" type="text" maxlength="255"
                            value="" />
                        <a href="javascript:search()">Search trailer on Youtube</a>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="link0">Link </label>
                        <input id="link0" class="form-control" name="link0" type="text" maxlength="255" value="" />
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="thumbnail">Thumbnail </label>
                        <input id="thumbnail" class="form-control" name="thumbnail" type="text" maxlength="255"
                            value="" />
                        <img id="thumbnail-img" src="" class="tn">
                        <div style="clear:both"></div>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="comment">Comment </label>
                        <textarea id="comment" class="form-control" name="comment"></textarea>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="creationtime">Created : <span id="creationtime-str">...</span></label>
                        <input type="hidden" id="creationtime" class="form-control" name="creationtime">
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="lastmodification">Modified : <span id="lastmodification-str">...</span></label>
                        <input type="hidden" id="lastmodification" class="form-control" name="lastmodification">
                    </div>
                    <input id="saveForm" class="btn btn-primary" type="submit" name="submit" value="Submit" />
                </form>
            </div>
            <div id="dumper" class="col-md-6"></div>
        </div>
    </div>
</body>

</html>